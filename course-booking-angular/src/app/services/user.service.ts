import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

import { User } from '@models/user';
import { compileDeclareClassMetadata } from '@angular/compiler';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.apiUrl + '/users';

  constructor(
      private http: HttpClient
  ) { }

  login(email: string, password: string): Observable<Object> { 
      return this.http.post(this.baseUrl + '/login', {email, password});
  }

  register(user: User): void { }

  enroll(courseId: number): void { }

}

